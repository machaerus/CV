.PHONY: help
help:
	cat Makefile

.PHONY: compile
compile:
	pdflatex main.tex
	mv main.pdf "Jan Szwagierczak CV.pdf"
